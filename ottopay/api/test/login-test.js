const assert = require('chai').expect;
const page = require('../page/login.js');
const readlineSync = require('readline-sync');



const testCase = {
 "positive" : {
    "login" : "As a User, I want to be able to Login to the apps",
    "inputotp" : "As a User, I want input the OTP",
    "loginotp" : "As a user, I want to able input OTP when Login for the first time"
 }
//  ,
//  "negative" : {
//     "noSearch" : "As a User, I should got error message when I send request without key of search",
//     "invalidApiKey" : "As a User, I should got error 401 when I send request with invalid API Key"
//  }
}

function delay(interval) 
{
   return it('should delay', done => 
   {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}

describe(`Ottopay Login`, () => {
 const deviceid = '35533810027112';
 const phone = '085600000002';
 const pin = '112233';
 const version = '87'
 const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

 it(`@get ${testCase.positive.login}`, async() => {
    
  const response = await page.loginop(deviceid, phone, pin,version);
  assert(response.status).to.equal(200);
  assert(response.body.meta.code).to.equal(200);

});
    
    it(`@get ${testCase.positive.loginotp}`, async() => {
        const otp_code = readlineSync.question('Input Your OTP CODE   : ', otp => {
            console.log(`Your OTP CODE ${otp}!`);
            readline.close();
          });
    const response = await page.loginotpop(deviceid, phone,version,otp_code);
    assert(response.status).to.equal(200);
    assert(response.body.meta.code).to.equal(200);
   
   })
}) 