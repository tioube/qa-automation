const assert = require('chai').expect;
const page = require('../page/login.js');
const merchant = require('../page/merchant_info.js');


const testCase = {
 "positive" : {
    "merchant_info" : "As a User, I want to be able to get Merchant Info",
    "merchant_feature" : "As a User, I want to be able to get Merchant Feature",
    "merchant_theme" : "As a User, I want to be able to get Merchant theme"
 }
//  ,
//  "negative" : {
//     "noSearch" : "As a User, I should got error message when I send request without key of search",
//     "invalidApiKey" : "As a User, I should got error 401 when I send request with invalid API Key"
//  }
}

function delay(interval) 
{
   return it('should delay', done => 
   {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}

describe(`Merchant Info `, () => {
 const deviceid = '35533810027112';
 const phone = '085600000002';
 const pin = '112233';
 const version = '87';
 

 before(async function(){
     
    resp = await page.loginop(deviceid, phone, pin, version);
    this.auth = resp.body.data.access_token;
    console.log("merchantauth = " + resp.body.data.access_token );
  }); 

it(`@get ${testCase.positive.merchant_info}`, async function() {
        const auth = this.auth;
        response = await merchant.merchantinfo(deviceid,auth);
        assert(response.status).to.equal(200);
        assert(response.body.meta.code).to.equal(200);
});

it(`@get ${testCase.positive.merchant_feature}`, async function() {
    const auth = this.auth;
    response = await merchant.merchantfeature(deviceid,auth);
    assert(response.status).to.equal(200);
    assert(response.body.meta.code).to.equal(200);
})

it(`@get ${testCase.positive.merchant_theme}`, async function() {
    const auth = this.auth;
    response = await merchant.merchanttheme(deviceid,auth);
    assert(response.status).to.equal(200);
    assert(response.body.meta.code).to.equal(200);
})



});


