const supertest = require('supertest');
const env = require('dotenv').config();

const apilogin = supertest(process.env.loginurl);
const apiloginotp = supertest(process.env.loginotpurl);


const loginop = (deviceid, phone, pin,version) => apilogin.post('')
 .set('Content-Type', 'application/json')
 .set('Accept', 'application/json')
 .set('Device-Id', deviceid)
 .send(
    {
        "firebase_token": "e2rHSwlrR2-nOJsvGObGTE:APA91bFphsNoL8nOr3R5V9zgep4lIGRiueWOfwf9Jqq4WKcD4Yb9MCnhlS8NTeSaU_RY4BimWh6ysIwMSU0VliRiUX6r2lDih21hYnR_lYA-Ma5OHkk0ZITVJOpOkZNRx9BHj6KDTOH4",
        "firebase_token_sandbox": null,
        "latitude": -6.31397401,
        "longitude": 106.87537699,
        "pin": pin,
        "user_id": phone,
        "version": version
      }
 )

 console.log(loginop);

 const loginotpop = (deviceid, phone,version,otp_code) => apiloginotp.post('')
 .set('Content-Type', 'application/json')
 .set('Accept', 'application/json')
 .set('Device-Id', deviceid)
 .send(
    {
        "firebase_token": "e2rHSwlrR2-nOJsvGObGTE:APA91bFphsNoL8nOr3R5V9zgep4lIGRiueWOfwf9Jqq4WKcD4Yb9MCnhlS8NTeSaU_RY4BimWh6ysIwMSU0VliRiUX6r2lDih21hYnR_lYA-Ma5OHkk0ZITVJOpOkZNRx9BHj6KDTOH4",
        "firebase_token_sandbox": null,
        "latitude": -6.31397401,
        "longitude": 106.87537699,
        "otp_code": otp_code,
        "user_id": phone,
        "version": version
      }
 )
 
module.exports = {
   loginop,loginotpop
}