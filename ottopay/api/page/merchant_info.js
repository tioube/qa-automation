const supertest = require('supertest');
const env = require('dotenv').config();

const apimerchantinfo = supertest(process.env.merchantinfourl);
const apimerchantfeature = supertest(process.env.merchantfeatureurl);
const apimerchanttheme = supertest(process.env.merchantthemeurl);

 const merchantinfo = (deviceid,auth) => apimerchantinfo.get('')
 .set('Content-Type', 'application/json')
 .set('Accept', 'application/json')
 .set('Authorization', 'Bearer '+ auth)
 .set('Device-Id', deviceid)

 const merchantfeature = (deviceid,auth) => apimerchantfeature.get('')
 .set('Content-Type', 'application/json')
 .set('Accept', 'application/json')
 .set('Authorization', 'Bearer '+ auth)
 .set('Device-Id', deviceid)
 
 const merchanttheme = (deviceid,auth) => apimerchanttheme.get('')
 .set('Content-Type', 'application/json')
 .set('Accept', 'application/json')
 .set('Authorization', 'Bearer '+ auth)
 .set('Device-Id', deviceid)
module.exports = {
   merchantinfo,merchantfeature,merchanttheme
}